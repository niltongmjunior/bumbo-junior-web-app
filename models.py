from bumbo_junior.orm import Table, Column


class Book(Table):
    author = Column(str)
    name = Column(str)
